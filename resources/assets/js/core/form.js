export class Form{

	constructor(model, endPoint)
	{
		this.endpoint =  config.apiEndPoint + endPoint;
		this.model = model;

		this.errors = new Errors([]	);
		console.log(this.errors)
		this.setModel();
	}

	setModel(){

		//set the value from the model to the class Form
		//so we can call Form.key instead of Form.model.key
		Object.keys(this.model).forEach(key =>{
			this[key] = this.model[key];
		})
	}

	getModel(){

		let data ={};
		
		Object.keys(this.model).forEach(key =>{
			data[key] = this[key];
		})

		return data;
	}

	submit(){

		this.errors.reset();
		
		return new Promise((resolve, reject) =>{

			axios.post(this.endpoint, this.getModel()).then(response=>{

				resolve(response.data);

			}).catch(ex =>{

				let status = ex.response.status;
				
				let data = ex.response.data;

				if(status == 422)
				{
					this.errors.set(data.errors);
					reject(422);
				}
				
				reject(data)

				

			});


		})

	}

}

export class Errors{

	constructor(errors){
		this.errors = errors;
		this.set(errors);
	}

	set(errors)
	{	
		this.errors = errors;
		Object.keys(errors).forEach(key =>{
			Vue.set(this,key, errors[key][0]);
		});
	}

	has(key)
	{
		return this[key] != undefined;
	}

	get(key)
	{
		return this[key];
	}

	clear(key)
	{	
		delete this[key];
	}

	reset()
	{
		console.log(this.errors);

		Object.keys(this.errors).forEach(key =>{
			console.log(key);
			Vue.delete(this, key);
		});
	}

}