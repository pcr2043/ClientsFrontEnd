export class Table{

	constructor(fields, endPoint){

		this.endpoint = config.apiEndPoint + endPoint;
		this.paginate=null;	
	}

	bind(){

		axios.get(this.endpoint).then(response =>{
			this.paginate = response.data;
		}).catch(ex =>{
			console.log("error loading data");
		});
	}

	first(){
		if(this.paginate.links.first != null)
		{
			this.endpoint= this.paginate.links.first;
			this.bind();
		}
	}

	prev(){
		
		if(this.paginate.links.prev != null)
		{
			this.endpoint= this.paginate.links.prev;
			this.bind();
		}
	}


	next(){

		if(this.paginate.links.next != null)
		{
			this.endpoint= this.paginate.links.next;
			this.bind();
		}
	}

	last(){
		if(this.paginate.links.last != null)
		{
			this.endpoint= this.paginate.links.last;
			this.bind();
		}
	}


}