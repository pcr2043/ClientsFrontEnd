
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');



//create window insance
window.Vue = require('vue');


import Vue from 'vue';
import VueRouter from 'vue-router';
window.Event = new Vue;

Vue.use(VueRouter);

import { Config } from './core/config.js';
window.config = new Config();

import { Form } from './core/form.js';
window.Form = Form;

import { Table } from './core/table.js';
window.Table = Table;

import {default as Swal} from 'sweetalert2'
window.Swal = Swal;
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


 const Login = require('./views/auth/login.vue')
 const Clients = require('./views/clients/index.vue')





 const router = new VueRouter({
 	history:false,
 	mode: 'history',
 	routes: [
 	{ path: '/', component: Clients, meta: { auth: true }},
 	{ path: '/login', component: Login, meta : { login: true} },
 	{ path: '/logout', component: Login, meta : { logout: true} },
 	{ path: '*', component: Login, meta: { not_found: true} },
 	]
 })

 router.beforeEach((to, from, next) =>{	
 	
 	if(to.meta.auth &&  app == undefined)
 	{	
 		next({ path: '/login'});		
 	}

 	if(to.meta.logout)
 	{
 		app.user = null;
 		localStorage.removeItem('clients.app.user')
 		next({ path: '/login'});	
 	}
 	
 	if(to.meta.not_found != undefined)
 	{
 		next({ path: '/login'});
 	}


 	next();


 });	

 let app =new Vue({
 	el: '#app',
 	data:{
 		user: null
 	},
 	methods:
 	{
 		setToken()
 		{
 			window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.user.api_token;
 		}
 	},
 	router,	
 	mounted(){

 		let user =  localStorage.getItem('clients.app.user');

 		if(user != undefined)
 		{
 			this.user = JSON.parse(user);
 			this.setToken();
 			router.push('/')	 			
 		}


 		Event.$on('update-user', (user_instance) =>{

 			localStorage.setItem('clients.app.user', JSON.stringify(user_instance));
 			this.user = user_instance;
 			this.setToken();
 			router.push('/')	
 		})

 	}
 })
